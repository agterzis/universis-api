import app from '../server/app';
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import Rule from '../server/models/rule-model';

const executeInTransaction = TestUtils.executeInTransaction;

describe('ExpandRule', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        // disable sql logging
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize(() => done());
        }
        // re-enable development env

        return done();
    });

    const COURSE_TYPE_RULE =
        {
            "id": 20000007,
            "target": "20000003",
            "targetType": "Program",
            "refersTo": "CourseType",
            "additionalType": "GraduateRule",
            "checkValues": "20000009",
            "ruleOperator": 8,
            "value1": "49",
            "value2": null,
            "value6": "1",
            "value7": null,
            "value9": null,
            "value10": "20000001, 20000002,20000003",
            "value11": "20000000,20000001",
            "ruleExpression": null,
            "programSpecialty": "1"
        };

    const COURSE_RULE =  {
        "id": 20004932,
        "target": "20004167",
        "targetType": "ProgramCourse",
        "refersTo": "Course",
        "additionalType": "ProgramCourseRegistrationRule",
        "checkValues": "20002532",
        "ruleOperator": 8,
        "value1": null,
        "value2": null,
        "value3": ".5",
        "value4": "1",
        "value5": null,
        "value6": null,
        "value7": null,
        "value8": null,
        "value9": null,
        "value10": null,
        "value11": null,
        "value12": null,
        "value13": null,
        "ruleExpression": null,
        "programSpecialty": null
    };

    const INTERNSHIP_RULE = {
            "id": 20009234,
            "target": "20000021",
            "targetType": "Program",
            "refersTo": "Internship",
            "additionalType": "GraduateRule",
            "checkValues": null,
            "ruleOperator": 0,
            "value1": "3",
            "value2": null,
            "value3": null,
            "value4": null,
            "value5": null,
            "value6": "1",
            "value7": null,
            "value8": null,
            "value9": null,
            "value10": null,
            "value11": null,
            "value12": null,
            "value13": null,
            "ruleExpression": null,
            "programSpecialty": "1"
        };

    const THESIS_RULE =    {
            "id": 20000002,
            "target": "20000006",
            "targetType": "Program",
            "refersTo": "Thesis",
            "additionalType": "GraduateRule",
            "checkValues": "20000001",
            "ruleOperator": 0,
            "value1": "1",
            "value2": null,
            "value3": null,
            "value4": null,
            "value5": null,
            "value6": "1",
            "value7": null,
            "value8": null,
            "value9": null,
            "value10": null,
            "value11": null,
            "value12": null,
            "value13": null,
            "ruleExpression": null,
            "programSpecialty": "-1"
        };

    const STUDENT_RULE =  {
            "id": 20000043,
            "target": "20000003",
            "targetType": "Program",
            "refersTo": "Student",
            "additionalType": "GraduateRule",
            "checkValues": "semester",
            "ruleOperator": 8,
            "value1": "10",
            "value2": null,
            "value3": null,
            "value4": null,
            "value5": null,
            "value6": null,
            "value7": null,
            "value8": null,
            "value9": null,
            "value10": null,
            "value11": null,
            "value12": null,
            "value13": null,
            "ruleExpression": null,
            "programSpecialty": "-1"
        };

    it('should expand course type', async () => {
       // await executeInTransaction(context, async () => {
            /**
             *
             * @type {Rule}
             */
            const rule = context.model('Rule').convert(COURSE_RULE);

            const expanded = await rule.expand('CourseRuleEx');
            console.log(JSON.stringify(expanded, null, 2));


            const flattened = await Rule.flatten( context, expanded,'CourseRuleEx');
            console.log(JSON.stringify(flattened, null, 2));

        expect(expanded.id).toBe(20000002);
        //});
    });
});

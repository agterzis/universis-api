import ActionStatusType from '../models/action-status-type-model';

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const context = event.model.context;
    const target = event.model.convert(event.target);
    // get request
    const request = await context.model(event.model.name).where('id').equal(event.target.id).silent().getItem();
    // get document configuration
    const config = await context.model('DocumentConfiguration')
        .find(request.object)
        .silent()
        .getItem();

    if (!config.autoCompleted) {
        return;
    }
    const actionStatus = await target.property('actionStatus').getItem();
    if (actionStatus.alternateName !== ActionStatusType.ActiveActionStatus) {
        // exit
        return;
    }
    Object.assign(target, {
        actionStatus: {
            alternateName: ActionStatusType.CompletedActionStatus
        }
    });
    await event.model.silent().save(target);
    Object.assign(event.target, target);
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}


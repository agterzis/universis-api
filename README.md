# UniverSIS
[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

This repository provides information on the API methods and the schema models.

Check them out!
https://api.universis.io/api-docs/

## How to install
  Check out the installation instructions in [INSTALL.md](INSTALL.md)

## Development

Clone repository:

    git clone https://gitlab.com/universis/universis-api.git
    cd universis-api

Checkout `next` branch:

    git checkout -b next origin/next

Install dependencies:

    npm ci

and serve:

    npm run serve

Universis api server will be started under http://localhost:5001/

Get a development token by using demo OAuth2 server of [universis.io](https://login.universis.io)

    curl --location 'https://users.universis.io/authorize' \
      --header 'Content-Type: application/x-www-form-urlencoded' \
      --data-urlencode 'username=registrar1@example.com' \
      --data-urlencode 'password=293EC246' \
      --data-urlencode 'client_id=6683492203869356' \
      --data-urlencode 'client_secret=UbAsFQprB7TuKDsZ2PMfrUk2Lo4ewUdY' \
      --data-urlencode 'grant_type=password' \
      --data-urlencode 'scope=registrar'

and start using api:

    curl --location 'http://localhost:5001/api/Courses' \
      --header 'Authorization: Bearer <access_token>'




## Services
Summary of available routes

* Instructors: https://gitlab.com/universis/universis-api/blob/master/server/routes/instructors.md

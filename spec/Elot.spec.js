import app from '../server/app';
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';


const executeInTransaction = TestUtils.executeInTransaction;

describe('Elot', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });

    it('should make elot-743 conversions', async() => {
        await executeInTransaction(context, async () => {
            expect(context).toBeInstanceOf(ExpressDataContext);
            const defaultLocale = context.getConfiguration().getSourceAt('settings/i18n/defaultLocale');
            if (defaultLocale !== 'el') {
                return;
            }
            // create a person
            const mockPerson = {
                familyName: "Παπαδόπουλος",
                givenName: "Ιωάννης",
                gender: {
                    alternateName: "male",
                },
                fatherName: "Κωνσταντίνος",
                motherName: null, // validate null also
                email: "email@example.com",
            };
            // save person
            const savePerson = await context.model('Person').silent().save(mockPerson);
            // fetch person
            const person = await context.model('Person')
                .where('id').equal(savePerson.id)
                .select('id', 'locales')
                .expand('locales')
                .silent()
                .getItem();
            // find en locale
            let enLocale = person.locales.find(locale => locale.inLanguage === 'en');
            expect(enLocale).toBeTruthy();
            expect(enLocale.familyName).toEqual('Papadopoulos');
            expect(enLocale.givenName).toEqual('Ioannis');
            expect(enLocale.fatherName).toEqual('Konstantinos');
            expect(enLocale.motherName).toBeFalsy();
            // update person
            savePerson.givenName = 'Γεώργιος';
            // validate null also
            savePerson.motherName = 'Μαρία';
            let updatePerson = await context.model('Person').silent().save(savePerson);
            updatePerson = await context.model('Person')
                .where('id').equal(updatePerson.id)
                .select('id', 'locales')
                .expand('locales')
                .silent()
                .getItem();
            enLocale = updatePerson.locales.find(locale => locale.inLanguage === 'en');
            expect(enLocale.familyName).toEqual('Papadopoulos');
            expect(enLocale.givenName).toEqual('Georgios');
            expect(enLocale.fatherName).toEqual('Konstantinos');
            expect(enLocale.motherName).toEqual('Maria');

        });
    });

});

import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Department = require('./department-model');
import StudyLevel = require('./study-level-model');
import GradeScale = require('./grade-scale-model');
import Semester = require('./semester-model');
import StudyProgramSpecialty = require('./study-program-specialty-model');

/**
 * @class
 */
declare class StudyProgram extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός του προγράμματος σπουδών
      */
     public id: number; 
     
     /**
      * @description Το τμήμα που προσφέρει το πρόγραμμα σπουδών
      */
     public department: Department|any; 
     
     /**
      * @description Κωδικός επιπέδου σπουδών
      */
     public studyLevel: StudyLevel|any; 
     
     /**
      * @description Ενεργό (ΝΑΙ/ΟΧΙ)
      */
     public isActive: boolean; 
     
     /**
      * @description Ονομασία προγράμματος σπουδών
      */
     public name: string; 
     
     /**
      * @description Συντομογραφία
      */
     public abbreviation: string; 
     
     /**
      * @description Ονομασία του προγράμματος για χρήση στις εκτυπώσεις (μπορεί να διαφέρει από την ονομασία)
      */
     public printName: string; 
     
     /**
      * @description Ο τίτλος πτυχίου που απονέμεται κατά το πέρας του προγράμματος
      */
     public degreeDescription?: string; 
     
     /**
      * @description Η κλίμακα βαθμολογίας του βαθμού που απονέμεται από το πρόγραμμα σπουδών
      */
     public gradeScale: GradeScale|any; 
     
     /**
      * @description Ο αριθμός των δεκαδικών ψηφίων του βαθμού πτυχίου
      */
     public decimalDigits: number; 
     
     /**
      * @description Αν το πρόγραμμα έχει δίδακτρα (ΝΑΙ/ΟΧΙ)
      */
     public hasFees: boolean; 
     
     /**
      * @description Ο τυπικός αριθμός εξαμήνων του προγράμματος.
      */
     public semesters: number; 
     
     /**
      * @description Το ελάχιστο εξάμηνο του φοιτητή προκειμένου να επιλέξει κατεύθυνση
      */
     public specialtySelectionSemester: Semester|any; 
     
     /**
      * @description Το σύνολο των κατευθύνσεων του προγράμματος σπουδών
      */
     public specialties?: Array<StudyProgramSpecialty|any>;

     getGraduationRules(): Promise<any>;

     setGraduationRules(items: any[]): Promise<any>;

     getInscriptionRules(): Promise<any>;

     setInscriptionRules(items: any[]): Promise<any>;

     getInternshipRules(): Promise<any>;

     setInternshipRules(items: any[]): Promise<any>;

}

export = StudyProgram;

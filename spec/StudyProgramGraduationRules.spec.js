import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import {INSTITUTE, DEPARTMENT, STUDY_PROGRAM, GRADE_SCALES} from './data';
import Institute from '../server/models/institute-model';
import StudyProgram from '../server/models/study-program-model';
import Department from '../server/models/department-model';
import GradeScale from "../server/models/grade-scale-model";
const executeInTransaction = TestUtils.executeInTransaction;
// eslint-disable-next-line no-unused-vars
const Rule = require('../server/models/rule-model');

describe('StudyProgramGraduationRules', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize(() => done());
        }
        return done();
    });

    it('should create study program graduation rules', async () => {
        await executeInTransaction(context, async ()=> {

            let institute = INSTITUTE;
            let department = DEPARTMENT;
            await context.model(Institute).silent().insert(institute);
            await context.model(Department).silent().insert(department);
            await context.model(GradeScale).silent().insert(GRADE_SCALES);
            /**
             * @type {StudyProgram}
             */
            let studyProgram = STUDY_PROGRAM;
            // get grade scale
            studyProgram.gradeScale = await context.model(GradeScale)
                .where('name').equal('0-10')
                .silent().getItem();
            await context.model(StudyProgram).silent().insert(studyProgram);
            expect(studyProgram.id).toBeTruthy();
            /**
             * @type {StudyProgram}
             */
            studyProgram = await context.model(StudyProgram)
                .where('id').equal(studyProgram.id)
                .expand('department')
                .silent()
                .getTypedItem();
            expect(studyProgram.id).toBeTruthy();
            expect(studyProgram.department.id).toBe(department.id);
            /**
             * @type {Array<Rule>}
             */
            let graduationRules = await studyProgram.getGraduationRules();
            expect(graduationRules).toBeInstanceOf(Array);
            expect(graduationRules.length).toBe(0);
            graduationRules.push({
                refersTo: 'Student',
                ruleOperator: {
                    alternateName: 'greaterThan'
                },
                checkValues: 'semester',
                value1: 8
            });
            await studyProgram.silent().setGraduationRules(graduationRules);
            graduationRules = await studyProgram.getGraduationRules();
            expect(graduationRules).toBeInstanceOf(Array);
            expect(graduationRules.length).toBeGreaterThan(0);
        });
    });

});

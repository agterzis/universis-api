import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {string} additionalType
 * @property {Action|any} action
 * @property {Course|any} course
 * @property {Student|any} student
 * @property {CourseExam|any} courseExam
 * @property {string} studentIdentifier
 * @property {number} semester
 * @property {string} familyName
 * @property {string} givenName
 * @property {number} examGrade
 * @property {string} formattedGrade
 * @property {number} previousGrade
 * @property {string} previousFormattedGrade
 * @property {GradeStatus|any} status
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('StudentGradeActionTrace')
class StudentGradeActionTrace extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = StudentGradeActionTrace;
import {TextUtils, TraceUtils} from '@themost/common/utils';
import {DataCacheStrategy} from "@themost/data/data-cache";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeExecute(event, callback) {
    try {
        const context = event.model.context;
        if (event.query) {
            if (event.query.$select) {
                //create hash
                const hash = TextUtils.toMD5(event.query), key = '/' + event.model.name + '/?query=' + hash + '&lang=' + event.model.context.locale;
                TraceUtils.debug('Getting data from cache [' + key + ']');
                return context.getConfiguration().getStrategy(DataCacheStrategy).get(key).then((result) => {
                    if (typeof result !== 'undefined') {
                        //delete expandables
                        if (event.emitter) {
                            delete event.emitter.$expand;
                        }
                        event.cached = true;
                        event.result = result;
                        return callback();
                    }
                    else {
                        return callback();
                    }
                }).catch((err)=> {
                    return callback(err);
                });
            }
        }
        callback();
    }
    catch (e) {
        callback(e)
    }
}

export function afterExecute(event, callback) {
    try {
        const context = event.model.context;
        if (event.query) {
            if (event.query.$select) {
                if (typeof event.result !== 'undefined' && !event.cached) {
                    //create hash
                    const hash = TextUtils.toMD5(event.query), key = '/' + event.model.name + '/?query=' + hash + '&lang=' + event.model.context.locale;;
                    TraceUtils.debug('Setting data to cache [' + key + ']');
                    context.getConfiguration().getStrategy(DataCacheStrategy).add(key, event.result);
                    return callback();
                }
            }
        }
        callback();
    }
    catch (e) {
        callback(e)
    }
}

export function afterSave(event, callback) {
    try {
        callback();
    }
    catch (e) {
        callback(e)
    }
}

import { ApplicationServiceAsListener } from "./application-service-as-listener";
import { DataError, DataNotFoundError, TraceUtils } from "@themost/common";
import { DataObjectState } from "@themost/data";
import { getMailer } from "@themost/mailer";

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	if (event.state !== DataObjectState.Update) {
		return;
	}
	const context = event.model.context,
		previous = event.previous,
		target = event.target;
	if (previous == null) {
		throw new DataError(
			"E_PREVIOUS",
			"The previous state of the object cannot be determined",
			null,
			"CreateStudentUserAction"
		);
	}
	// get target action status (after save)
	const targetActionStatus = await context
		.model("CreateStudentUserAction")
		.where("id")
		.equal(target.id)
		.select("actionStatus/alternateName")
		.silent()
		.value();
	// if this is not a change from active to completed action, exit
	if (
		!(
			previous.actionStatus &&
			previous.actionStatus.alternateName === "ActiveActionStatus" &&
			targetActionStatus === "CompletedActionStatus"
		)
	) {
		return;
	}
	const studentId =
		typeof target.student === "object" ? target.student.id : target.student;
	// get student
	const student = await context
		.model("Student")
		.where("id")
		.equal(studentId)
		.expand("user", "person")
		.getItem();
	if (student == null) {
		throw new DataNotFoundError(
			"The specified student cannot be found or is inaccesible"
		);
	}
	if (student.user == null) {
		throw new DataError(
			"E_NOENT",
			"The student user cannot be empty after a completed create student user action",
			null,
			"Student"
		);
	}
	// validate sms service
	const service = context.getApplication().getService(function SmsService() {});
	if (service == null) {
		throw new Error(
			"Invalid application configuration. Messaging service is missing or is in an inaccessible state."
		);
	}
	// get activation code
	const activationCode = target.activationCode;
	if (activationCode == null) {
		throw new DataError(
			"E_ACTIVATION_CODE",
			"Activation code cannot be empty at this context",
			null,
			"CreateStudentUserAction"
		);
	}
	// get sms template
	const smsTemplate = await context
		.model("MailConfiguration")
		.where("target")
		.equal("NotifyStudentUserAction")
		.silent()
		.getItem();
	if (smsTemplate == null) {
		// throw error
		throw new DataNotFoundError(
			"Sms template for account activation notification is missing. User cannot be notified for this action.",
			null,
			"MailConfiguration"
		);
	}
	// get mailer
	const mailer = getMailer(context);
	const result = await new Promise((resolve, reject) => {
		mailer
			.template(smsTemplate.template)
			.test(true)
			.send(
				{
					model: {
						student,
						action: event.target,
					},
				},
				(err, body) => {
					if (err) {
						TraceUtils.error(
							"NotifyStudentUserAction",
							"An error occurred while trying to send an sms notification for account activation."
						);
						return reject(err);
					}
					// send message
					return resolve(body);
				}
			);
	});
	await context
		.model("SMS")
		.silent()
		.save({
			sender: context.user,
			recipient: student.user.id,
			body: result && result.html,
		});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			TraceUtils.error(err);
			return callback(err);
		});
}

class SendSmsAfterStudentUserCreation extends ApplicationServiceAsListener {
	constructor(app) {
		super(app);
		// install the target listener as member of event listeners of target model
		this.install("CreateStudentUserAction", __filename);
	}

	async send(context, student) {
		const CreateStudentUserActions = context.model("CreateStudentUserAction");
		// get action and validate
		const target = await CreateStudentUserActions
            .where('student').equal(student)
            .and('actionStatus/alternateName').equal('CompletedActionStatus')
            .orderByDescending('id')
            .silent()
            .getItem();
		if (target == null) {
			TraceUtils.error(
				"SendSmsAfterCreateStudentUser",
				"An attempt of sending sms notification to a student failed because the relevant CreateStudentUserAction was not found."
			);
			throw new DataNotFoundError(
				"Create user action cannot be found or is inaccessible",
				null,
				"CreateStudentUserAction"
			);
		}
		await afterSaveAsync({
			model: CreateStudentUserActions, // set event model
			state: DataObjectState.Update, // set state to update to force sending message
			target: target, // set action as target object
			previous: {
				actionStatus: {
					alternateName: "ActiveActionStatus", // override previous state
				},
			},
		});
		return true;
	}
}

export {
    SendSmsAfterStudentUserCreation
}

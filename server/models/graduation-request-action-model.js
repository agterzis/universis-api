import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataNotFoundError} from '@themost/common';
import {DataConflictError, ValidationResult} from "../errors";
let RequestAction = require('./request-action-model');
/**
 * @class

 * @property {number} id
 */
@EdmMapping.entityType('GraduationRequestAction')
class GraduationRequestAction extends RequestAction {
    /**
     * @constructor
     */
    constructor() {
        super();
        // this event is going to be used to handle claim operation and
        // create a StudentDeclareAction where the initiator is this action
        // and owner is the agent that claimed this action
        this.on('claimed', (event, cb) => {
            (async () => {

            })().then(() => {
                return cb();
            }).catch((err) => {
                return cb(err);
            });

        });
    }


    async declareStudent() {
        try {
            const id = this.getId();
            // try to get action by initiator
            const item = await this.context.model('StudentDeclareAction').where('initiator').equal(id).getItem();
            // validate action state
            if (item == null) {
                // convert current action
                const newItem = await this.context.model('GraduationRequestAction')
                    .where('id').equal(this.getId())
                    .select(
                        'student as object',
                        'agent as owner',
                        'graduationEvent/graduationYear as declaredYear',
                        'graduationEvent/graduationPeriod as declaredPeriod',
                        'graduationEvent/startDate as graduationDate'
                    )
                    .getItem();
                // validate that item exists
                if (newItem == null) {
                    throw new DataNotFoundError('The current action cannot be found or is inaccessible', null, 'GraduationRequestAction');
                }
                // validate owner
                if (newItem.owner == null) {
                    throw new DataConflictError('The operation cannot be completed because initiator agent cannot be found or has not be set yet.', null, 'GraduationRequestAction');
                }
                Object.assign(newItem, {
                    initiator: this.getId(), // set initiator (this action)
                    actionStatus: { // set action status to active
                        alternateName: 'ActiveActionStatus'
                    }
                });
                // and finally save action
                const action = await this.context.model('StudentDeclareAction').save(newItem);
                return action;
            }
            else
            {
                return item;
            }
        }
        catch (err) {
            throw (err);
        }
    }

    @EdmMapping.func('initiatorOf', EdmType.CollectionOf('StudentDeclareAction'))
    async initiatorOf() {
        return [];
    }

    /**
     *
     */
    @EdmMapping.func("validate", ValidationResult)
    async validateRequest() {
        try {
            // check student graduation rules
            const id = this.getId();
            const request = await this.context.model('GraduationRequestAction')
                .where('id').equal(id).expand(
                    {
                        name: 'graduationEvent',
                        options:
                            {
                                $expand: 'attachmentTypes($expand=attachmentType)'
                            }
                    },
                    {
                        name: 'attachments',
                        options:
                            {
                                $expand: 'actions($expand=actionStatus)'
                            }
                    }
                )
                .levels(5)
                .getItem();
            if (request) {
                /**
                 *
                 * @type {Student|*}
                 */
                const student = this.context.model('Student').convert(request.student);
                const graduationRules = await student.checkGraduationRules(null);

                // check attachmentTypes
                const attachments = request.attachments;
                const attachmentTypes = request.graduationEvent && request.graduationEvent.attachmentTypes;
                let attachmentTypeResult;
                if (attachmentTypes.length > 0) {
                    for (let i = 0; i < attachmentTypes.length; i++) {
                        const attachmentType = attachmentTypes[i];
                        // check if attachment exists
                        attachmentType.validationResult = new ValidationResult(attachments.filter(x => {
                            return x.attachmentType === attachmentType.attachmentType.id && x.actions && x.actions.length > 0 && x.actions[0].actionStatus.alternateName === 'CompletedActionStatus';
                        }).length > 0);
                    }
                    attachmentTypeResult = new ValidationResult(attachmentTypes.filter(x => {
                        return x.validationResult.success === false;
                    }).length === 0);
                    attachmentTypeResult.type = 'RequiredAttachmentRule';
                    attachmentTypeResult.attachmentTypes = attachmentTypes;
                }
                //build finalResult
                request.graduationValid = graduationRules && graduationRules.finalResult && graduationRules.finalResult.success;
                request.attachmentValid = attachmentTypeResult && attachmentTypeResult.success;
                // save results to request
                await this.context.model('GraduationRequestAction').silent().save(request);
                const validationResults = [];
                validationResults.push(graduationRules.finalResult);
                if (attachmentTypeResult) {
                    validationResults.push(attachmentTypeResult);
                }
                let finalResult = new ValidationResult(validationResults.filter(x => {
                    return x.success === false;
                }).length === 0);
                finalResult.validationResults = validationResults;
                return finalResult;
            } else {
                throw new DataNotFoundError();
            }

        } catch (err) {
            throw (err);
        }
    }
}
module.exports = GraduationRequestAction;

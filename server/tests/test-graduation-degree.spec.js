import app from '../app';
import Student from '../models/student-model';
import path from "path";
import util from "util";
import fs from "fs";


const writeFileAsync = util.promisify(fs.writeFile);

describe('test calculation rules', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('should calculate graduation grade', async () => {
        const students = await context.model('Student').where('studentStatus/alternateName')
            .equal('graduated').and('graduationYear').equal('2017')
            .silent().select('id').getAllItems();
        //  const students = await context.model('Student').where('id').equal('20086823').silent().getItems();

        for (let i = 0; i < students.length; i++) {
            const student = students[i];
            const response = await calculateStudentGraduationDegree(student.id);
            student.graduationGrade = response.graduationGrade;
            student.studentGraduationGrade = response.studentGraduationGrade;
            if (response.studentGraduationGrade === response.graduationGrade) {
                student.success = true;
            } else {
                student.success = false;
                console.log(`Student ${students[i].id} should be checked!!!`);
            }
            if (i % 100 === 0 && i > 0) {
                const filename = `.tmp/graduated${i}.json`;
                await writeFileAsync(path.resolve(__dirname, filename), JSON.stringify(students), 'utf8');
            }
        }
        const failed = students.filter(student => {
            return student.success === false;
        });

        await writeFileAsync(path.resolve(__dirname, '.tmp/graduated.json'), JSON.stringify(students), 'utf8');


    });

    async function calculateStudentGraduationDegree(studentId) {
        /**
         * @type Student
         */
            // const student = await Student.getMe(context).getTypedItem();
        const student = await context.model('Student').where('id').equal(studentId).silent().getTypedItem();
        const response = await student.calculateGraduationDegree();
        return response;
    }


});
